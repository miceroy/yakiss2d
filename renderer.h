#pragma once
#include <kgfw/Object.h>	// Include kgfw::Object to be used as a base class
#include <string>
#include <map>
#include <glm/vec2.hpp>
#include <vector>

class Camera;
class Shader;
class SpriteBatch;
class Texture;
class GLFWwindow;
class Sprite;

class Renderer : public kgfw::Object {
public:
    Renderer(size_t m_playAreaX, size_t m_playAreaY);

    ~Renderer();

    Sprite* createSprite(const std::string& textureFileName, size_t layer = 0, const glm::vec2& clipStart = glm::vec2(0.0f), const glm::vec2& clipEnd = glm::vec2(1.0f));
    Sprite* createSprite(const std::string& textureFileName, size_t layer, float clipStartX, float clipStartY, float clipEndX, float clipEndY);

    void beginRender(GLFWwindow* window, float clearColorRed, float clearColorGreen, float clearColorBlue);

    void addSprites(const std::vector<Sprite*>& sprites);

    void endRender(GLFWwindow* window);

    Camera* getMainCamera() const {
        return m_mainCamera;
    }

    Shader* getShader() const {
        return m_shader;
    }

    Texture* getTexture(const std::string& fileName);
    SpriteBatch* getSpriteBatch(size_t layer, Texture* texture);
private:
    size_t m_playAreaX;
    size_t m_playAreaY;
    Shader*						m_shader;		// Pointer to the Shader object
    Camera*                     m_mainCamera;   // Main camera
    std::vector< std::map<Texture*, SpriteBatch*> >				m_layeredSpriteBatches;	// SpriteBatch.

    std::map<std::string, Texture*>					m_textures;		// Textures

};
