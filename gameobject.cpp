#include "gameobject.h"
#include <glm/gtx/transform.hpp>	// glm transform functions.

GameObject::GameObject(const char* const functionName)
    : Object(functionName) {
    // Initialize transform
    setPosition(glm::vec2(0.0));
    setRotation(0.0f);
    setScaling(glm::vec2(1.0f));
}

GameObject::~GameObject() {
}

void GameObject::setPosition(const glm::vec2& position) {
    m_position = position;
}

void GameObject::setRotation(float angleInRadians) {
    m_angleZInRadians = angleInRadians;
}

void GameObject::setScaling(const glm::vec2& scale) {
    m_scale = scale;
}

const glm::vec2& GameObject::getPosition() const {
    return m_position;
}

float GameObject::getRotation() const {
    return m_angleZInRadians;
}

const glm::vec2& GameObject::getScaling() const {
    return m_scale;
}

glm::mat4 GameObject::getModelMatrix() const {
    return glm::translate(glm::mat4(1.0f), glm::vec3(m_position.x, m_position.y,0.0f))
            * glm::rotate(m_angleZInRadians, glm::vec3(0.0f, 0.0f, 1.0f))
            * glm::scale(glm::mat4(1.0f), glm::vec3(m_scale.x, m_scale.y, 1.0f));
}


