#pragma once
#include <kgfw/Object.h>	// Include kgfw::Object to be used as a base class
#include <glad/gl.h>		// Include glad
#include <string>


class Texture : public kgfw::Object {
public:
    Texture(const std::string& fileName);
	Texture(int width, int height, int nrChannels, const GLubyte* data);
	~Texture();

	GLuint getTextureId() const;

    void setData(int width, int height, int nrChannels, const GLubyte* data);

    int getNrChannels() const {
        return m_nrChannels;
    }

    int getWidth() const {
        return m_width;
    }

    int getHeight() const {
        return m_height;
    }
private:
	GLuint				m_textureId;	// Texture id
    int                 m_width;
    int                 m_height;
    int                 m_nrChannels;
};
