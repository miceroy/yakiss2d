#include "texture.h"
#include <kgfw/GLUtils.h>	// Include GLUtils for checkGLError
// Include STB-image library
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <stdexcept>

Texture::Texture(const std::string& fileName)
    : Object(__FUNCTION__)
    , m_textureId(0)
    , m_width(0)
    , m_height(0)
    , m_nrChannels(0) {
    // Create texture
    glGenTextures(1, &m_textureId);
    checkGLError();
    // Load the data for our texture using stb-image stbi_load-function
    GLubyte* data = stbi_load(fileName.c_str(), &m_width, &m_height, &m_nrChannels, 0);
    if(data==0) {
        auto errorStr = "Error: Could not open file: \"" + fileName + "\"";
        printf("%s\n", errorStr.c_str());
        throw std::runtime_error(errorStr);
    }
    setData(m_width, m_height, m_nrChannels, data);
}

Texture::Texture(int width, int height, int nrChannels, const GLubyte* data)
    : Object(__FUNCTION__)
    , m_textureId(0)
    , m_width(0)
    , m_height(0)
    , m_nrChannels(0) {
    // Create texture
    glGenTextures(1, &m_textureId);
    checkGLError();
    setData(width, height, nrChannels, data);
}

Texture::~Texture() {
	glDeleteTextures(1, &m_textureId);
}

GLuint Texture::getTextureId() const {
	return m_textureId;
}


void Texture::setData(int width, int height, int nrChannels, const GLubyte* data) {
    m_width = width;
    m_height = height;
    m_nrChannels = nrChannels;
    // Bind it for use
    glBindTexture(GL_TEXTURE_2D, m_textureId);
    checkGLError();
    // set the texture data as RGBA
    glTexImage2D(GL_TEXTURE_2D, 0, nrChannels == 3 ? GL_RGB : GL_RGBA, width, height, 0, nrChannels == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, data);
    checkGLError();
    // set the texture wrapping options to repeat
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    checkGLError();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    checkGLError();
    // set the texture filtering to nearest (disabled filtering)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    checkGLError();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    checkGLError();
}
