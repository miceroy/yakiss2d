#include <stdio.h>			// Include stdio.h, which contains printf-function
#include <kgfw/Object.h>	// Include kgfw::Object
#include <glad/gl.h>		// Include glad
#include <GLFW/glfw3.h>		// Include glfw
#include <kgfw/GLUtils.h>	// Include GLUtils for checkGLError
#include "shader.h"			// Include Plane-class.
//#include "plane.h"			// Include Shader-class.
#include <vector>			// Include std::vector
#include "camera.h"			// Include Camera-class.
#include <algorithm>		// std::sort
#include "texture.h"		// Include Texture-class.
#include "spritebatch.h"	// Include SpriteBatch-class.
#include "sprite.h"	// Include Sprite-class.
#include "renderer.h"
#include "physics.h"



class Application : public kgfw::Object
{
public:
	Application()
        : Object(__FUNCTION__) {
        size_t playAreaX = 32;
        m_renderer = new Renderer(playAreaX, 24);
        m_physics = new Physics(0.0f, -10.0f);

        /*
        for( size_t x=0; x<playAreaX; ++x) {
            auto sprite = m_renderer->createSprite("checkerboard_48.png", 0);
            sprite->setPosition(x,0);
            m_gameObjects.push_back(sprite);
        }*/

        for( size_t x=0; x<playAreaX; ++x) {
            auto sprite = m_renderer->createSprite("checkerboard_48.png", 0);
            sprite->setPosition(x,23);
            m_gameObjects.push_back(sprite);
        }

        for( size_t y=0; y<23; ++y) {
            auto sprite = m_renderer->createSprite("checkerboard_48.png", 0);
            sprite->setPosition(0,y);
            m_gameObjects.push_back(sprite);
        }

        for( size_t y=0; y<23; ++y) {
            auto sprite = m_renderer->createSprite("checkerboard_48.png", 0);
            sprite->setPosition(31,y);
            m_gameObjects.push_back(sprite);
        }

        for( float x=3.5; x<31; x+=4) {
            auto sprite = m_renderer->createSprite("box_cyan_192.png", 1);
            sprite->setScaling(4,1);
            sprite->setPosition(x,20);
            m_gameObjects.push_back(sprite);

            sprite = m_renderer->createSprite("box_green_192.png", 1);
            sprite->setScaling(4,1);
            sprite->setPosition(x,19);
            m_gameObjects.push_back(sprite);

            sprite = m_renderer->createSprite("box_magenta_192.png", 1);
            sprite->setScaling(4,1);
            sprite->setPosition(x,18);
            m_gameObjects.push_back(sprite);

            sprite = m_renderer->createSprite("box_cyan_192.png", 1);
            sprite->setScaling(4,1);
            sprite->setPosition(x,17);
            m_gameObjects.push_back(sprite);

            sprite = m_renderer->createSprite("box_green_192.png", 1);
            sprite->setScaling(4,1);
            sprite->setPosition(x,16);
            m_gameObjects.push_back(sprite);

            sprite = m_renderer->createSprite("box_magenta_192.png", 1);
            sprite->setScaling(4,1);
            sprite->setPosition(x,15);
            m_gameObjects.push_back(sprite);
        }

        {
            auto sprite = m_renderer->createSprite("sphere_red_48.png", 1);
            sprite->setPosition(15.5,1.5);
            m_gameObjects.push_back(sprite);
        }

        {
            auto sprite = m_renderer->createSprite("box_blue_192.png", 1);
            sprite->setScaling(4,1);
            sprite->setPosition(15.5,0.5);
            m_gameObjects.push_back(sprite);
        }
	}

	~Application() {
		for (auto gameObject : m_gameObjects) {
			delete gameObject;
		}

        // Delete Renderer
        delete m_renderer;
        delete m_physics;

	}

    void render(GLFWwindow* window) {
        m_renderer->beginRender(window, 0.2f, 0.0f, 0.2f);

        m_renderer->addSprites(m_gameObjects);

        m_renderer->endRender(window);
	}

    bool update(float deltaTime) {
		float i = 1.0f;
        m_physics->update(deltaTime);
		for (auto gameObject : m_gameObjects) {
        //    gameObject->setRotation(gameObject->getRotation() + i++*3.14f*deltaTime);
		}
        return true;
	}


private:
    std::vector<Sprite*>	m_gameObjects;	// Vector to gameobject pointers.
    Renderer*         			m_renderer;		// Renderer.
    Physics*                    m_physics;

};


int main(void) {
    // Set c++-lambda as error call back function for glfw.
	glfwSetErrorCallback([](int error, const char* description) {
		fprintf(stderr, "Error %d: %s\n", error, description);
	});

	// Try to initialize glfw
	if (!glfwInit()) {
		return -1;
	}

	// Create window and check that creation was succesful.
    GLFWwindow* window = glfwCreateWindow(2*640, 2*480, "Yakiss2d - Demo Game", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	// Set current context
	glfwMakeContextCurrent(window);
	// Load GL functions using glad
	gladLoadGL(glfwGetProcAddress);

	// Create application
    // Global pointer to the application
    Application* app = new Application();
    glfwSetWindowUserPointer(window, app);
	// Specify the key callback as c++-lambda to glfw
	glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
        (void)scancode;
        (void)mods;
        Application* app = static_cast<Application*>(glfwGetWindowUserPointer(window));
		// Close window if escape is pressed by the user.
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
			glfwSetWindowShouldClose(window, GLFW_TRUE);
		}
	});

	// Get time using glfwGetTime-function, for delta time calculation.
	float prevTime = (float)glfwGetTime();
	while (!glfwWindowShouldClose(window)) {
		// Render the game frame and swap OpenGL back buffer to be as front buffer.
        app->render(window);

		// Poll other window events.
		glfwPollEvents();

		// Compute application frame time (delta time) and update application
		float curTime = (float)glfwGetTime();
		float deltaTime = curTime - prevTime;
		prevTime = curTime;
        if( !app->update(deltaTime) ) {
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        }
	}

	// Delete application
	delete app;
	app = 0;

	// Destroy window
	glfwDestroyWindow(window);

	// Terminate glfw
	glfwTerminate();

	return 0;
}
