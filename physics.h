#pragma once
#include <kgfw/Object.h>	// Include kgfw::Object to be used as a base class
#include <box2d/box2d.h>
#include "body.h"

class Physics : public kgfw::Object, public b2ContactListener, public b2ContactFilter {
public:
    Physics(float gravityX, float gravityY)
        : Object(__FUNCTION__)
        , m_physicsWorld(0)
        , m_contactFilters() {
        m_physicsWorld = new b2World(b2Vec2(gravityX, gravityY));
        m_physicsWorld->SetContactFilter(this);
        m_physicsWorld->SetContactListener(this);
    }

    ~Physics() {
        delete m_physicsWorld;
    }

    Body* createStaticBody(size_t type) {
        Body* body = new Body(type);
        return body;
    }

    virtual bool ShouldCollide (b2Fixture *fixtureA, b2Fixture *fixtureB) {
        Body* bodyA = static_cast<Body*>((void*)fixtureA->GetBody()->GetUserData().pointer);
        Body* bodyB = static_cast<Body*>((void*)fixtureB->GetBody()->GetUserData().pointer);
        if(bodyA->getType() >= m_contactFilters.size()) return false;
        if(bodyB->getType() >= m_contactFilters[bodyA->getType()].size()) return false;
        return m_contactFilters[bodyA->getType()][bodyB->getType()];
    }


    virtual void BeginContact(b2Contact* contact) {
        Body* bodyA = static_cast<Body*>((void*)contact->GetFixtureA()->GetBody()->GetUserData().pointer);
        Body* bodyB = static_cast<Body*>((void*)contact->GetFixtureB()->GetBody()->GetUserData().pointer);
        bodyA->beginContact(bodyB);
        bodyB->beginContact(bodyA);
    }

    virtual void EndContact(b2Contact* contact) {
        Body* bodyA = static_cast<Body*>((void*)contact->GetFixtureA()->GetBody()->GetUserData().pointer);
        Body* bodyB = static_cast<Body*>((void*)contact->GetFixtureB()->GetBody()->GetUserData().pointer);
        bodyA->endContact(bodyB);
        bodyB->endContact(bodyA);
    }

    void enableContacts(size_t t0, size_t t1) {
        size_t maxSize = t0 >= t1 ? t0 : t1;
        if( maxSize >= m_contactFilters.size() ){
            m_contactFilters.resize(maxSize);
            for(auto contactFilterRow : m_contactFilters) {
                contactFilterRow.resize(maxSize);
            }
        }
        m_contactFilters[t0][t1] = true;
        m_contactFilters[t1][t0] = true;
    }

    void update(float deltaTime) {
        m_physicsWorld->Step(deltaTime, 6, 2);
    }

private:    
    b2World*                    m_physicsWorld;
    std::vector< std::vector<bool> > m_contactFilters;

};
