#pragma once
#include "gameobject.h"     // Include base class
#include <glm/vec2.hpp>

class SpriteBatch;

class Sprite : public GameObject {
public:
    Sprite(SpriteBatch* spriteBatch, const glm::vec2& clipStart = glm::vec2(0.0f), const glm::vec2& clipEnd = glm::vec2(1.0f))
        : GameObject(__FUNCTION__)
        , m_spriteBatch(spriteBatch)
        , m_clipStart(clipStart)
        , m_clipEnd(clipEnd) {
    }

    Sprite(SpriteBatch* spriteBatch, float clipStartX, float clipStartY, float clipEndX, float clipEndY)
        : GameObject(__FUNCTION__)
        , m_spriteBatch(spriteBatch)
        , m_clipStart(clipStartX,clipStartY)
        , m_clipEnd(clipEndX,clipEndY) {
    }

    ~Sprite() {

    }

    glm::vec2 getClipStart() const {
        return m_clipStart;
    }

    glm::vec2 getClipEnd() const {
        return m_clipEnd;
    }

    SpriteBatch* getSpriteBatch() const {
        return m_spriteBatch;
    }

private:
    SpriteBatch* m_spriteBatch;
    glm::vec2 m_clipStart;
    glm::vec2 m_clipEnd;
};
