#pragma once
#include <kgfw/Object.h>	// Include kgfw::Object to be used as a base class
#include <glad/gl.h>		// Include glad
#include <vector>
#include <glm/glm.hpp>

class Shader;
class Sprite;
class Camera;
class Texture;

class SpriteBatch : public kgfw::Object {
public:
	SpriteBatch();
	~SpriteBatch();

	void clear();
    void addSprite(Sprite* sprite, Camera* camera);
    void render(Shader* shader, Texture* texture);


private:
	std::vector<glm::vec3>	m_positions;
	std::vector<glm::vec2>	m_texCoords;
	bool					m_needUpdateBuffers;
	GLuint					m_positionsVbo;	// Handle to positions VBO
	GLuint					m_texCoordsVbo;	// Handle to positions VBO
	GLuint					m_vao;			// Handle to VAO
};
