#include "renderer.h"
#include "camera.h"
#include <glad/gl.h>		// Include glad
#include <GLFW/glfw3.h>		// Include glfw
#include <kgfw/GLUtils.h>	// Include GLUtils for checkGLError
#include "shader.h"
#include "spritebatch.h"
#include "texture.h"
#include "sprite.h"

Renderer::Renderer(size_t playAreaX, size_t playAreaY)
    : Object(__FUNCTION__)
    , m_shader(0)
    , m_playAreaX(playAreaX)
    , m_playAreaY(playAreaY)
    , m_mainCamera(new Camera(0, playAreaX, 0, playAreaY)) {
    m_mainCamera->setPosition(-0.5,-0.5);
    const char *vertexShaderSource =
        "#version 330 core\n"
        "layout (location = 0) in vec3 in_position;\n"
        "layout (location = 1) in vec2 in_texCoord;\n"
        "out vec2 texCoord;"
        "void main()\n"
        "{\n"
        "   texCoord = in_texCoord;\n"
        "   gl_Position = vec4(in_position.x, in_position.y, in_position.z, 1.0);\n"
        "}";

    const char *fragmentShaderSource =
        "#version 330 core\n"
        "uniform sampler2D texture0;\n"
        "in vec2 texCoord;"
        "out vec4 FragColor;\n"
        "void main()\n"
        "{\n"
        "   FragColor = texture2D(texture0, texCoord);\n"
        "}";

    // Build and compile our shader program
    m_shader = new Shader(vertexShaderSource, fragmentShaderSource);
}

Renderer::~Renderer() {
    delete m_mainCamera;
    delete m_shader;

    for(auto it : m_textures) {
        delete it.second;
    }

    for( auto spriteBatches : m_layeredSpriteBatches ) {
        for(auto it : spriteBatches) {
            delete it.second;
        }
    }
}

Sprite* Renderer::createSprite(const std::string& textureFileName, size_t layer, const glm::vec2& clipStart, const glm::vec2& clipEnd){
    auto spriteBatch = getSpriteBatch(layer, getTexture(textureFileName));
    return new Sprite(spriteBatch, clipStart,clipEnd);
}

Sprite* Renderer::createSprite(const std::string& textureFileName, size_t layer, float clipStartX, float clipStartY, float clipEndX, float clipEndY) {
    auto spriteBatch = getSpriteBatch(layer, getTexture(textureFileName));
    return new Sprite(spriteBatch, clipStartX, clipStartY, clipEndX, clipEndY);
}

void Renderer::beginRender(GLFWwindow* window, float clearColorRed, float clearColorGreen, float clearColorBlue) {
    // Query the size of the framebuffer (window content) from glfw.
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    // Setup the opengl wiewport (i.e specify area to draw)
    glViewport(0, 0, width, height);
    checkGLError();
    // Set color to be used when clearing the screen
    glClearColor(clearColorRed, clearColorGreen, clearColorBlue, 1.0f);
    checkGLError();
    // Clear the screen
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    checkGLError();

    for( auto spriteBatches : m_layeredSpriteBatches ) {
        for(auto it : spriteBatches) {
            it.second->clear();
        }
    }
}

void Renderer::addSprites(const std::vector<Sprite*>& sprites) {
    for (auto sprite : sprites) {
        sprite->getSpriteBatch()->addSprite(sprite, getMainCamera());
    }
}


void Renderer::endRender(GLFWwindow* window) {
    // Enable blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for( auto spriteBatches : m_layeredSpriteBatches ) {
        for(auto it : spriteBatches) {
            it.second->render(getShader(), it.first);
        }
    }

    glfwSwapBuffers(window);
}


Texture* Renderer::getTexture(const std::string& fileName) {
    if( m_textures.find(fileName) == m_textures.end()) {
        m_textures[fileName] = new Texture(fileName);
    }
    return m_textures[fileName];
}


SpriteBatch* Renderer::getSpriteBatch(size_t layer, Texture* texture) {
    if(layer >= m_layeredSpriteBatches.size()) {
        m_layeredSpriteBatches.resize(layer+1);
    }
    auto& spriteBatches = m_layeredSpriteBatches[layer];
    if( spriteBatches.find(texture) == spriteBatches.end()) {
        spriteBatches[texture] = new SpriteBatch();
    }
    return spriteBatches[texture];
}
