#pragma once
#include <kgfw/Object.h>	// Include kgfw::Object to be used as a base class
#include <glm/glm.hpp>      // Include glm

class GameObject : public kgfw::Object {
public:
    GameObject(const char* const functionName);
    ~GameObject();

    void setPosition(const glm::vec2& position);

    void setPosition(float x, float y) {
        setPosition(glm::vec2(x,y));
    }
    void setRotation(float angleInRadians);
    void setScaling(const glm::vec2& scale);
    void setScaling(float scaleX, float scaleY) {
        setScaling(glm::vec2(scaleX,scaleY));
    }

    const glm::vec2& getPosition() const;
    float getRotation() const;
    const glm::vec2& getScaling() const;

    glm::mat4 getModelMatrix() const;

private:
    // Model position, rotation and scale
    glm::vec2 m_position;           // Store position of plane here
    float m_angleZInRadians;        // Store angle of plane here
    glm::vec2 m_scale;              // Store scale of plane here
};
