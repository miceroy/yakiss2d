#pragma once
#include <kgfw/Object.h>	// Include kgfw::Object to be used as a base class

class Body : public kgfw::Object {
public:
    Body(size_t type)
        : Object(__FUNCTION__)
        , m_type(type) {
    }

    ~Body();

    size_t getType()  const {
        return m_type;
    }

    void beginContact(Body* other) {

    }

    void endContact(Body* other) {

    }

private:
    size_t m_type;

};
